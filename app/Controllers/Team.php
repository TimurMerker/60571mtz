<--?php namespace App\Controllers;

use App\Models\TeamModel;

class Team extends BaseController


{
    public function index() //Обображение всех записей
    {
        $model = new TeamModel();
        $data ['Team'] = $model->getTeam();
        echo view('player/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        $model = new TeamModel();
        $data ['Team'] = $model->getTeam($id);
        echo view('player/view', $this->withIon($data));
    }
}

