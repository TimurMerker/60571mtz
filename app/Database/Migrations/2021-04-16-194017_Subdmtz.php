<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Subdmtz extends Migration
{
    public function up()
    {
        // activity_type


        if (!$this->db->tableexists('team'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),

            ));
            // create table
            $this->forge->createtable('team', TRUE);
        }

        if (!$this->db->tableexists('player'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'team_id' => array('type' => 'int', 'null' => TRUE),
                'fullname' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'position' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),

            ));
            $this->forge->addForeignKey('team_id','team','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('player', TRUE);
        }

    }
    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->droptable('team');
        $this->forge->droptable('player');

    }
}
