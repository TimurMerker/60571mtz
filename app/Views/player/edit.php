<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('player/update'); ?>
        <input type="hidden" name="id" value="<?= $player["id"] ?>">

        <div class="form-group">
            <label for="name">Полное Имя</label>
            <input type="text" class="form-control <?= ($validation->hasError('fullname')) ? 'is-invalid' : ''; ?>" name="fullname"
                   value="<?= $player["fullname"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('fullname') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="name">Позиция в комманде</label>
            <input type="text" class="form-control <?= ($validation->hasError('position')) ? 'is-invalid' : ''; ?>" name="position"
                   value="<?= $player["position"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('position') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="team_id">Команда:</label>
            <select class="form-control <?= ($validation->hasError('team_id')) ? 'is-invalid' : ''; ?>"
                    name='team_id'
                    onChange="" id="team_id">
                <option value="-1">Выберите команду</option>
                <?php foreach ($team as $item): ?>
                    <option value="<?= ($item['id']) ?>" <?php if ($player["team_id"] == $item['id']) echo "selected"; ?>>
                        <?= esc($item['name']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('team_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture_url">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
                   name="picture"
                   id="picture_url"
                   value="<?= $player["picture_url"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('picture_url') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>