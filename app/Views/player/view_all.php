<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все Игроки</h2>

        <?php if (!empty($player) && is_array($player)) : ?>

            <?php foreach ($player as $item): ?>

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-body">

                                    <?php if(is_null($item['picture_url'])): ?>
                                        <img  class="card-img" src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.gofreedownload.net%2F3%2Ffootballer-silhouette-113422.jpg&f=1&nofb=1" alt="">
                                    <?php else : ?>
                                        <img  src="<?= esc($item['picture_url']); ?>" alt="<?= esc($item['fullname']); ?> " class="card-img">
                                    <?php endif; ?>
                                <h5 class="card-title" style="font-weight: bold"><?= esc($item['fullname']); ?></h5>
                                <p class="card-text"><?= esc($item['position']); ?></p>
                                <a href="<?= base_url()?>/index.php/player/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти рейтинги.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>
