<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($player) && is_array($player)) : ?>
            <h2>Все рейтинги:</h2>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group1','my_page') ?>
                <?= form_open('player/viewAllWithTeams', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-outline-success" type="submit">На странице</button>
                </form>
                <?= form_open('player/viewAllWithTeams',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit">Найти</button>
                </form>
            </div>
                <?php foreach ($player as $item): ?>

                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="card-body d-flex justify-content-center align-items-center">
                                    <?php if(is_null($item['picture_url'])): ?>
                                        <img class="mr-3" width="75px" src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.gofreedownload.net%2F3%2Ffootballer-silhouette-113422.jpg&f=1&nofb=1" alt="">
                                    <?php else : ?>
                                        <img class="mr-3"  width="75px" src="<?= esc($item['picture_url']); ?>" alt="<?= esc($item['fullname']); ?> ">
                                    <?php endif; ?>
                                    <div><h5 class="card-title" style="font-weight: bold"><?= esc($item['fullname']); ?></h5>
                                    <p class="card-text"><?= esc($item['position']); ?></p>
                                    <p class="card-text"><?= esc($item['name']); ?></p>
                                    <a href="<?= base_url()?>/player/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                                    <a href="<?= base_url()?>/player/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                                    <a href="<?= base_url()?>/player/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            <?php else : ?>
                <p>Невозможно найти рейтинги.</p>
            <?php endif ?>

    </div>
<?= $this->endSection() ?>