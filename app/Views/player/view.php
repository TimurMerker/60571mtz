<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($player)) :?>


        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <?php if(is_null($player['picture_url'])): ?>
                        <img class="card-img"   src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.gofreedownload.net%2F3%2Ffootballer-silhouette-113422.jpg&f=1&nofb=1" alt="">
                    <?php else : ?>
                        <img   src="<?= esc($player['picture_url']); ?>" alt="<?= esc($player['fullname']); ?> " class="card-img">
                    <?php endif; ?>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight: bold"><?= esc($player['fullname']); ?></h5>
                        <p class="card-text" style="font-weight: bold"><?= esc($player['position']); ?></p>

                        <b>id-игрока:</b><p class="card-text" ><?= esc($player['id']); ?></p>
                        <b>id-комманды:</b><h6 class="card-text"><?= esc($player['team_id']);?></h6>


                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>Рейтинг не найден.</p>
    <?php endif ?>

</div>
<?= $this->endSection() ?>
