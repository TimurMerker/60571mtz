<?php namespace App\Models;
use CodeIgniter\Model;
class TeamModel extends Model
{
    protected $table = 'team'; //таблица, связанная с моделью
    protected $allowedFields = ['name'];
    public function getTeam($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}
