<?php namespace App\Models;
use CodeIgniter\Model;
class PlayerModel extends Model
{
    protected $table = 'player'; //таблица, связанная с моделью
    protected $allowedFields = ['fullname', 'position', 'team_id', 'picture_url'];
    public function getPlayer($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['player.id' => $id])->first();
    }
    public function getPlayerWithTeam($id = null,$search='')
    {
        $builder = $this->select('*, player.id')
            ->join('team','team.id = player.team_id')
            ->like('name', $search,'both', null, true)
            ->orlike('fullname',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['player.id' => $id])->first();
        }
        return $builder;
    }
//    public function getReisWithMarshryt($id = null, $search = '')
//    {
//        $builder = $this->select('*, reis.id')
//            ->join('marshryt', 'marshryt.id = reis.id_marshryt')
//            ->like('tarif', $search, 'both', null, true)
//            ->orlike('naimen', $search, 'both', null, true);
//        if (!is_null($id)) {
//            return $builder->where(['reis.id' => $id])->first();
//        }
//        return $builder;
//    }
   // public function GetPlayerByTeam($Team_id=null)
}
