-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Фев 28 2021 г., 20:07
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571mtz`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Goal`
--

CREATE TABLE `Goal` (
  `id` int NOT NULL COMMENT 'Goal_ID',
  `id_match` int NOT NULL,
  `id_player` int NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Goal`
--

INSERT INTO `Goal` (`id`, `id_match`, `id_player`, `time`) VALUES
(1, 1, 1, '00:09:09'),
(2, 2, 6, '01:15:18'),
(3, 3, 13, '00:23:19'),
(6, 4, 17, '00:37:19'),
(7, 5, 24, '02:16:04'),
(8, 8, 1, '00:52:31'),
(9, 9, 21, '00:31:08'),
(10, 10, 7, '03:02:14');

-- --------------------------------------------------------

--
-- Структура таблицы `Match`
--

CREATE TABLE `Match` (
  `id` int NOT NULL COMMENT 'Match id',
  `id_team1` int NOT NULL,
  `id_team2` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Match`
--

INSERT INTO `Match` (`id`, `id_team1`, `id_team2`) VALUES
(1, 1, 2),
(2, 2, 3),
(3, 3, 4),
(4, 4, 5),
(5, 5, 1),
(6, 4, 1),
(7, 2, 5),
(8, 3, 1),
(9, 3, 5),
(10, 4, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `player`
--

CREATE TABLE `Player` (
  `id` int NOT NULL,
  `Team_id` int NOT NULL COMMENT 'ID из таблицы Team',
  `FullName` varchar(255) NOT NULL COMMENT 'FIO',
  `Position` varchar(255) NOT NULL COMMENT 'Role'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `player`
--

INSERT INTO `Player` (`id`, `Team_id`, `FullName`, `Position`) VALUES
(1, 1, 'Lionel Messi', 'Forward'),
(2, 1, 'Antuan Grizzman', 'Center'),
(3, 1, 'Ansu Fati', 'Kicker'),
(4, 1, 'Philippe Coutinho', 'Offensive Guard'),
(5, 1, 'Ronald Arauho', 'Middle linebacker'),
(6, 2, 'Olivie Girou', 'Forward'),
(7, 2, 'Timo Werner', 'Forward'),
(8, 2, 'Hakim Ziuech', 'Guard'),
(9, 2, 'Kai Havertz', 'Guard'),
(10, 2, 'Edouard Mendi', 'Keeper'),
(11, 3, 'Bukayo Ayoyinka Saka', 'Guard'),
(12, 3, 'Martin Ødegaard', 'Guard'),
(13, 3, 'Nicolas Pépé', 'Forward'),
(14, 3, 'Kieran Tierney', 'Guard'),
(15, 3, 'Mathew David Ryan', 'Keeper'),
(16, 4, 'Sergio Ramos García', 'Guard'),
(17, 4, 'Eden Michael Hazard', 'Forward'),
(18, 4, 'Karim Mostafa Benzema', 'Forward'),
(19, 4, 'Marcelo Vieira', 'Guard'),
(20, 4, 'Thibaut Nicolas Marc Courtois', 'Keeper'),
(21, 5, 'Diogo Jota', 'Forward'),
(22, 5, 'Alisson Ramses Becker', 'Keeper'),
(23, 5, 'Virgil van Dijk', 'Guard'),
(24, 5, 'Sadio Mané', 'Forward'),
(25, 5, 'Ozan Muhammed Kabak', 'Guard');

-- --------------------------------------------------------

--
-- Структура таблицы `Team`
--

CREATE TABLE `Team` (
  `id` int NOT NULL COMMENT 'ID_team',
  `Name` varchar(255) NOT NULL COMMENT 'Team_Name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Team`
--

INSERT INTO `Team` (`id`, `Name`) VALUES
(1, 'FCB'),
(2, 'CHELSEA'),
(3, 'ARSENAL'),
(4, 'Real Madrid'),
(5, 'Liverpool');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Goal`
--
ALTER TABLE `Goal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_match` (`id_match`),
  ADD KEY `id_player` (`id_player`);

--
-- Индексы таблицы `Match`
--
ALTER TABLE `Match`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_team1` (`id_team1`),
  ADD KEY `id_team2` (`id_team2`);

--
-- Индексы таблицы `player`
--
ALTER TABLE `Player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Team_id` (`Team_id`);

--
-- Индексы таблицы `Team`
--
ALTER TABLE `Team`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Goal`
--
ALTER TABLE `Goal`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'Goal_ID', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `Match`
--
ALTER TABLE `Match`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'Match id', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `player`
--
ALTER TABLE `Player`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `Team`
--
ALTER TABLE `Team`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID_team', AUTO_INCREMENT=6;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Goal`
--
ALTER TABLE `Goal`
  ADD CONSTRAINT `Goal_ibfk_1` FOREIGN KEY (`id_match`) REFERENCES `Match` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Goal_ibfk_2` FOREIGN KEY (`id_player`) REFERENCES `Player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Match`
--
ALTER TABLE `Match`
  ADD CONSTRAINT `Match_ibfk_1` FOREIGN KEY (`id_team1`) REFERENCES `Team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Match_ibfk_2` FOREIGN KEY (`id_team2`) REFERENCES `Team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `player`
--
ALTER TABLE `Player`
  ADD CONSTRAINT `Player_ibfk_1` FOREIGN KEY (`Team_id`) REFERENCES `Team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
